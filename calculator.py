# Copyright (c) 2018 matthiasschaffrath
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

"""
Calculator library containing basic math operations
"""


def add(first_term, second_term):
    return first_term + second_term


def subtract(first_term, second_term):
    return first_term - second_term


def multiply(first_term, second_term):
    return first_term * second_term
